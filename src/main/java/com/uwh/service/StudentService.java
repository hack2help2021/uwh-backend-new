package com.uwh.service;

import java.util.List;
import java.util.Optional;

import com.uwh.domain.Student;

/**
 * 
 * @author abdus
 * @version 1.0
 * @Description Student Service  
 */
public interface StudentService {

    List<Student> listAllStudents();

    Optional<Student> getStudentById(Integer id);

    Student saveStudent(Student student);

    void deleteStudent(Integer id);

	Optional<Student> findByRoleAndUidAndUidTypeAndPassword(String role,String uid,String uidType,String password);

}
