package com.uwh.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.data.domain.Page;

import com.uwh.api.request.ReportRequestDTO;
import com.uwh.api.response.CoursesDto;
import com.uwh.api.response.ReportsDTO;
import com.uwh.domain.Student;
import com.uwh.domain.StudentDetails;

public interface ReportsService {
	
	Page<StudentDetails> getReports(ReportRequestDTO reportRequestDTO) throws IllegalAccessException, InvocationTargetException;

	ReportsDTO generatePDF(ReportRequestDTO reportRequestDTO) throws IllegalAccessException, InvocationTargetException;
	
	ReportsDTO generateExcel(ReportRequestDTO reportRequestDTO) throws IllegalAccessException, InvocationTargetException;

}
