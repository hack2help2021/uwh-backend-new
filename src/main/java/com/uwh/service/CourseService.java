package com.uwh.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.uwh.api.request.CourseUpdateDto;
import com.uwh.api.response.CoursesDto;

public interface CourseService {
	List<CoursesDto> findAllCoursesOfPartner(Long partnerId) throws IllegalAccessException, InvocationTargetException;

	CoursesDto saveCourse(CoursesDto partnersDto) throws IllegalAccessException, InvocationTargetException;

	List<CoursesDto> findAllCourses() throws IllegalAccessException, InvocationTargetException;

	CoursesDto enrollStudentToCourse(Long courseId, List<Long> studentIds);

	CoursesDto updateCourse(CourseUpdateDto coursesDto);
}
