package com.uwh.service;

import java.util.List;

import com.uwh.api.response.JobsDTO;
import com.uwh.api.response.JobsUpdateDto;

public interface JobService {

    JobsDTO getJobDetailsById(Long JobId);

    JobsDTO saveJob(JobsDTO job);

    void deleteJob(Long id);

	JobsDTO enrollStudentToJob(Long jobId, List<Long> studentIds);

	JobsDTO updateStudentJobStatus(Long id, String status);

	List<JobsDTO> findAll();

	JobsDTO updateJob(JobsUpdateDto jobsDTO);
}
