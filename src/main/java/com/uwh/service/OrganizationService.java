package com.uwh.service;

import java.util.List;

import com.uwh.api.response.OrganizationDto;

public interface OrganizationService {

	List<OrganizationDto> getOrganizations();

}
