package com.uwh.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uwh.api.response.StudentDto;
import com.uwh.domain.Batches;
import com.uwh.domain.Courses;
import com.uwh.domain.Student;
import com.uwh.repositories.CountStudentRepository;
import com.uwh.service.CountStudentService;

@Repository
@Service
public class CountStudentServiceImpl implements CountStudentService {
	

    @Autowired 
    private CountStudentRepository countStudents;

	public int countStudents() {
		
		 return countStudents.countStudents();
	}
}

