package com.uwh.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uwh.api.response.JobsDTO;
import com.uwh.api.response.JobsUpdateDto;
import com.uwh.api.response.StudentDto;
import com.uwh.domain.Job;
import com.uwh.domain.Student;
import com.uwh.domain.StudentJobDetails;
import com.uwh.exceptions.BadRequestException;
import com.uwh.repositories.JobRepository;
import com.uwh.repositories.StudentJobDetailsRepository;
import com.uwh.repositories.StudentRepository;
import com.uwh.service.JobService;

/**
 * @author Sirisha
 */
@Service
public class JobServiceImpl implements JobService {

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	StudentJobDetailsRepository studentJobDetailsRepository;

	@Override
	public JobsDTO getJobDetailsById(Long JobId) {
		JobsDTO dto = new JobsDTO();
		Optional<Job> jobs = jobRepository.findById(JobId);
		if (jobs.isPresent()) {
			BeanUtils.copyProperties(jobs.get(), dto);
//			List<StudentJobDetails> jobDetails = jobs.get().getStudents();
			List<StudentJobDetails> jobDetails = studentJobDetailsRepository.findAllByJobId(JobId);
			List<StudentDto> dtos = new ArrayList<StudentDto>();
			for (StudentJobDetails studentJobDetails : jobDetails) {
				StudentDto studentDto = new StudentDto();
				BeanUtils.copyProperties(studentJobDetails.getStudent(), studentDto);
				dtos.add(studentDto);
			}
			dto.setStudents(dtos);
		}
		return dto;
	}

	@Override
	public List<JobsDTO> findAll() {
		List<JobsDTO> job = new ArrayList<>();
		List<com.uwh.domain.Job> jobs = jobRepository.findAll();
		for (Job job2 : jobs) {
			JobsDTO dto = new JobsDTO();
			BeanUtils.copyProperties(job2, dto);
			job.add(dto);
		}
		return job;
	}

	@Override
	public JobsDTO saveJob(JobsDTO job) {
		// TODO Auto-generated method stub
		Job entity = new Job();
		BeanUtils.copyProperties(job, entity);
		entity = jobRepository.save(entity);
		job.setId(entity.getId());
		return job;
	}

	@Override
	public void deleteJob(Long id) {
		// TODO Auto-generated method stub
		jobRepository.deleteById(id);
	}

	@Override
	public JobsDTO enrollStudentToJob(Long jobId, List<Long> studentIds) {
		JobsDTO jobsDTO = new JobsDTO();
		List<Student> students = studentRepository.findAllByIdIn(studentIds);
		Optional<Job> job = jobRepository.findById(jobId);

		if (job.isPresent()) {
			List<StudentJobDetails> studentJobDetails = new ArrayList<StudentJobDetails>();
			for (Student student : students) {
				StudentJobDetails jobDetails = StudentJobDetails.builder().student(student).job(job.get())
						.appliedDate(LocalDate.now().toString()).status("APPLIED").build();
				studentJobDetails.add(jobDetails);
			}
			studentJobDetailsRepository.saveAll(studentJobDetails);
			BeanUtils.copyProperties(job.get(), jobsDTO);
		} else {
			throw new BadRequestException("Job Not Found");
		}
		return jobsDTO;
	}

	@Override
	public JobsDTO updateStudentJobStatus(Long id, String status) {
		JobsDTO jobsDTO = new JobsDTO();
		Optional<StudentJobDetails> optional = studentJobDetailsRepository.findById(id);
		if (optional.isPresent()) {
			optional.get().setStatus(status);
			studentJobDetailsRepository.save(optional.get());
		}
		BeanUtils.copyProperties(optional.get().getJob(), jobsDTO);
		return jobsDTO;
	}

	@Override
	public JobsDTO updateJob(JobsUpdateDto jobsDTO) {
		// TODO Auto-generated method stub
		JobsDTO job = new JobsDTO();
		Optional<Job> optional = jobRepository.findById(jobsDTO.getId());
		if (optional.isPresent()) {
		Job entity = optional.get();
		BeanUtils.copyProperties(jobsDTO, entity);
		entity = jobRepository.save(entity);
		BeanUtils.copyProperties(entity, job);
		}
		return job;
	}

}
