package com.uwh.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uwh.domain.Student;
import com.uwh.repositories.StudentRepository;
import com.uwh.service.StudentService;

/**
 * @author abdus
 * Student service implement.
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	private StudentRepository studentRepository;

	@Autowired
	public void setStudentRepository(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public List<Student> listAllStudents() {
		List<Student> students = new ArrayList<>();
		studentRepository.findAll().forEach(students::add);

		return students;
	}

	@Override
	public Optional<Student> getStudentById(Integer id) {

		return studentRepository.findById(new Long(id));
	}

	@Override
	@Transactional
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public void deleteStudent(Integer id) {
		studentRepository.deleteById(new Long(id));
	}


	@Override
	public Optional<Student> findByRoleAndUidAndUidTypeAndPassword(String role,String uid,String uidType,String password){
		// TODO Auto-generated method stub
		return studentRepository.findByRoleAndUidAndUidTypeAndPassword(role,uid,uidType,password);
	}

	

}
