package com.uwh.service.impl;

import java.lang.reflect.InvocationTargetException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.uwh.api.request.ReportRequestDTO;
import com.uwh.api.response.ReportsDTO;
import com.uwh.domain.Student;
import com.uwh.domain.StudentDetails;
import com.uwh.domain.StudentPage;
import com.uwh.repositories.StudentCriteriaRepository;
import com.uwh.service.ReportsService;

@Service
public class ReportsServiceImpl implements ReportsService{
	
	@Autowired
	private StudentCriteriaRepository studentCriteriaRepository;

	@Override
	public Page<StudentDetails> getReports(ReportRequestDTO reportRequestDTO)
			throws IllegalAccessException, InvocationTargetException {
		//return studentCriteriaRepository.findAllWithFilters(studentPage, reportRequestDTO);
		return studentCriteriaRepository.findAllWithFilters(new StudentPage(), reportRequestDTO);
	}

	@Override
	public ReportsDTO generatePDF(ReportRequestDTO reportRequestDTO)
			throws IllegalAccessException, InvocationTargetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReportsDTO generateExcel(ReportRequestDTO reportRequestDTO)
			throws IllegalAccessException, InvocationTargetException {
		// TODO Auto-generated method stub
		return null;
	}

}
