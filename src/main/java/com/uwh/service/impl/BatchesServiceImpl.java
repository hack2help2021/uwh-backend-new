package com.uwh.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.uwh.api.response.BatchesDTO;
import com.uwh.domain.Batches;
import com.uwh.domain.Courses;
import com.uwh.domain.Student;
import com.uwh.domain.StudentBatch;
import com.uwh.domain.StudentCourse;
import com.uwh.repositories.BatchesRepository;
import com.uwh.repositories.CoursesRepository;
import com.uwh.repositories.StudentBatchRepository;
import com.uwh.repositories.StudentCourseRepository;
import com.uwh.service.BatchesService;
import com.uwh.utils.DtoConverter;

@Service
public class BatchesServiceImpl implements BatchesService {
	
	@Autowired
	private BatchesRepository batchesRepository;
	
	@Autowired
	private CoursesRepository coursesRepository;
	
	@Autowired
	private StudentCourseRepository studentCourseRepository;
	
	@Autowired
	private StudentBatchRepository studentBatchRepository;

	@Override
	public List<BatchesDTO> findAllBatchByCourseId(Long couserId)
			throws IllegalAccessException, InvocationTargetException {
		return DtoConverter.convertToBatchesDto(batchesRepository.findAllByCoursesId(couserId));
	}

	@Override
	public BatchesDTO saveBatch(BatchesDTO batchesDTO) throws IllegalAccessException, InvocationTargetException {
		Batches batches = new Batches();
		BeanUtils.copyProperties(batchesDTO, batches);
		Courses courses = coursesRepository.findById(batchesDTO.getCourses().getId()).get();
		batches.setCourses(courses);
		batches.setStatus("NEW");
		batches = batchesRepository.save(batches);
		
		Pageable pageable = PageRequest.of(0, 20);
		Page<StudentCourse> studentCourses = studentCourseRepository.findAllByCoursesIdAndStatus(courses.getId(), "ENROLLED", pageable);
		List<Student> students = studentCourses.stream().map(StudentCourse::getStudent).collect(Collectors.toList());
		List<StudentBatch> studentBatchs = new ArrayList<StudentBatch>();
		for (Student student : students) {
			StudentBatch studentBatch = new StudentBatch();
			studentBatch.setBatches(batches);
			studentBatch.setStudent(student);
			studentBatchs.add(studentBatch);
		}
		studentBatchRepository.saveAll(studentBatchs);
		studentCourses.forEach(x -> x.setStatus("BATCH_CREATED"));
		studentCourseRepository.saveAll(studentCourses);
		batchesDTO.setId(batches.getId());
		return batchesDTO;
	}

}
