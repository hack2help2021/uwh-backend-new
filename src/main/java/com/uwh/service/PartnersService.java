package com.uwh.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.uwh.api.response.PartnersDto;

public interface PartnersService {
    List<PartnersDto> findAllPartners() throws IllegalAccessException, InvocationTargetException;

	PartnersDto savePartner(PartnersDto partnersDto);

	PartnersDto updatePartner(PartnersDto partnersDto);
}
