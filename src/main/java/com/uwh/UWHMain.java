package com.uwh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UWHMain {

    public static void main(String[] args) {
        SpringApplication.run(UWHMain.class, args);
    }

}
