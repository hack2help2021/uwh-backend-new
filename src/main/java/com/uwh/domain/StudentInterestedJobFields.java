package com.uwh.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Table(name = "StudentInterestedJobFields")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder



public class StudentInterestedJobFields {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;
    private String interestedJobFieldName;//Healthcare,Teaching,Mechanics,IT
	

}
