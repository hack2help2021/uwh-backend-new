package com.uwh.domain;

import java.sql.Timestamp;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentDetails {
	
	private Student student;
	private String courseName;
	private String status;
	private String partnerName;
	private String orgName;
	private String jobStatus;
	private String jobName;
	private String jobType;
	private String jobLocation;
	private String jobDescription;
	private Double maxSalary;
	/*
	 * private Double maxSalary; private String jobDescription; private String
	 * jobLocation;
	 */

}
