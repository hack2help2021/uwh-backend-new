package com.uwh.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.lang.reflect.InvocationTargetException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.request.ReportRequestDTO;
import com.uwh.api.response.ReportsDTO;
import com.uwh.domain.Student;
import com.uwh.domain.StudentDetails;
import com.uwh.service.ReportsService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;



@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "Reports resource")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH + "/reports")
public class ReportsController {
	
	@Autowired
	private ReportsService reportsService;
	
	@PostMapping("/view")
	@ApiOperation("This api is to get the list of students by applying filters")
	public ResponseEntity<Page<StudentDetails>> getReports(@RequestBody ReportRequestDTO requestDTO) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(reportsService.getReports(requestDTO), OK);
	}
	
	@PostMapping("/generateExcel")
	@ApiOperation("This api is to get the add partners")
	public ResponseEntity<ByteArrayResource> generateExcel(@RequestBody ReportRequestDTO requestDTO) throws IllegalAccessException, InvocationTargetException {
		return null;
	}
	
	/*
	 * @GetMapping("/generatePDF")
	 * 
	 * @ApiOperation("This api is to get the add partners") public
	 * ResponseEntity<ReportsDTO> generatePDF(@RequestBody ReportRequestDTO
	 * requestDTO) throws IllegalAccessException, InvocationTargetException { return
	 * new ResponseEntity<>(reportsService.generatePDF(requestDTO), OK); }
	 */

}
