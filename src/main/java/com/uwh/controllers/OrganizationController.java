package com.uwh.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.response.OrganizationDto;
import com.uwh.service.OrganizationService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Organization controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "Organization resource")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH + "/org")
public class OrganizationController {

	@Autowired
	private OrganizationService organizationService;
	
	@GetMapping("/all")
	@ApiOperation("This api is to get the list of organizations")
	public ResponseEntity<List<OrganizationDto>> getOrganizations() {
		return new ResponseEntity<>(organizationService.getOrganizations(), OK);
	}

}
