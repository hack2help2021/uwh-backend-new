package com.uwh.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.response.PartnersDto;
import com.uwh.service.PartnersService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

/**
 * Organization controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "Partner resource")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH + "/partner")
public class PartnerController {

	@Autowired
	private PartnersService partnersService;
	
	@GetMapping("/all")
	@ApiOperation("This api is to get the list of partners")
	public ResponseEntity<List<PartnersDto>> getOrganizations() throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(partnersService.findAllPartners(), OK);
	}
	
	@PostMapping("/save")
	@ApiOperation("This api is to get the add partners")
	public ResponseEntity<PartnersDto> savePartner(@RequestBody PartnersDto partnersDto) {
		return new ResponseEntity<>(partnersService.savePartner(partnersDto), OK);
	}
	
	@PutMapping("/update")
	@ApiOperation("This api is to get the update partners")
	public ResponseEntity<PartnersDto> updatePartner(@RequestBody PartnersDto partnersDto) {
		return new ResponseEntity<>(partnersService.updatePartner(partnersDto), OK);
	}
}
