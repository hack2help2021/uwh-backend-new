package com.uwh.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.request.StudentIds;
import com.uwh.api.response.JobsDTO;
import com.uwh.api.response.JobsUpdateDto;
import com.uwh.service.JobService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

/**
 * Organization controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "Jobs resource")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH + "/job")
public class JobController {

	@Autowired
	private JobService jobService;
	
	@GetMapping("/all/{JobId}")
	@ApiOperation("This api is to get the list of Jobs")
	public ResponseEntity<JobsDTO> getJobDetailsById(@PathVariable Long JobId) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(jobService.getJobDetailsById(JobId), OK);
	}
	
	@GetMapping("/all/")
	@ApiOperation("This api is to get the list of Jobs")
	public ResponseEntity<List<JobsDTO>> findAll() throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(jobService.findAll(), OK);
	}
	
	@PostMapping("/save")
	@ApiOperation("This api is to get the add job")
	public ResponseEntity<JobsDTO> saveJob(@RequestBody JobsDTO jobsDTO) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(jobService.saveJob(jobsDTO), OK);
	}
	
	@PostMapping("/enrollStudent/{jobId}")
	@ApiOperation("This api is to enroll students for a particular job")
	public ResponseEntity<JobsDTO> enrollStudentToJob(@PathVariable Long jobId, @RequestBody StudentIds studentIds) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(jobService.enrollStudentToJob(jobId, studentIds.getStudentIds()), OK);
	}
	
	@PutMapping("/jobDetails/status/{id}/{status}")
	public ResponseEntity<JobsDTO> updateStudentJobStatus(@PathVariable Long id, @PathVariable String status) {
		return new ResponseEntity<>(jobService.updateStudentJobStatus(id, status), OK);
	}
	
	@PutMapping("/update")
	@ApiOperation("This api is to get the update job")
	public ResponseEntity<JobsDTO> saveJob(@RequestBody JobsUpdateDto jobsDTO) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(jobService.updateJob(jobsDTO), OK);
	}
}
