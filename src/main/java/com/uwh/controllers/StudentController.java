package com.uwh.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.domain.Student;
import com.uwh.service.StudentService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;

/**
 * 
 * @author abdus
 *
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "UWH Backend APIs")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH)
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@PostMapping(value = "/student")
	public ResponseEntity<Student> createStudent(@RequestBody Student student) {

		return new ResponseEntity<>(studentService.saveStudent(student),HttpStatus.OK);
	}
	
	@GetMapping(value = "/students")
	public ResponseEntity<List<Student>> getStudents() {		
		System.out.println("Returning Students:");
		return new ResponseEntity<>(studentService.listAllStudents(),HttpStatus.OK);
	}
	
	@GetMapping(value = "/students/{id}")
	public Optional<Student> getStudentById(@PathVariable Integer id) {
		// model.addAttribute("product", productService.getProductById(id));
		return studentService.getStudentById(id);
	}

	
	@PutMapping(value = "/student")
	public ResponseEntity<Optional<Student>> updateStudent(@RequestBody Student student) {

		return new ResponseEntity<>(Optional.ofNullable(studentService.saveStudent(student)),HttpStatus.OK);
	}


}
