package com.uwh.controllers;

import static org.springframework.http.HttpStatus.OK;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uwh.api.response.BatchesDTO;
import com.uwh.service.BatchesService;
import com.uwh.utils.Constant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

/**
 * Batches controller.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Api(value = "Batches resource")
@RestController
@RequestMapping(value = Constant.BASE_CONTEXT_PATH + "/batches")
public class BatchesController {
	
	@Autowired
	private BatchesService batchesService;
	
	@GetMapping("/all/{courseId}")
	@ApiOperation("This api is to get the list of partners")
	public ResponseEntity<List<BatchesDTO>> findBatches(@PathVariable Long courseId) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(batchesService.findAllBatchByCourseId(courseId), OK);
	}
	
	@PostMapping("/save")
	@ApiOperation("This api is to save the batch")
	public ResponseEntity<BatchesDTO> saveBatch(@RequestBody BatchesDTO batchesDTO) throws IllegalAccessException, InvocationTargetException {
		return new ResponseEntity<>(batchesService.saveBatch(batchesDTO), OK);
	}

	
}
