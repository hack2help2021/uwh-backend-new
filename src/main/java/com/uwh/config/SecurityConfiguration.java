package com.uwh.config;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.uwh.domain.Organization;
import com.uwh.repositories.OrganizationRepository;

/**
 * @author Ramakrishna Veldandi
 * @project UWH
 */

@Configuration
public class SecurityConfiguration {
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**/*").allowedOrigins("*");
			}
		};
	}
	
	@PostConstruct
	public void updateDb() {
		if (CollectionUtils.isEmpty(organizationRepository.findAll())) {
		organizationRepository.save(Organization.builder()
				.address("Flat No: 401, 4th Floor, Lahari Park Apartment, Somajiguda, Hyderabad - 500 082")
				.email("contact@unitedwayhyderabad.org")
				.orgName("United Way of Hyderabad")
				.phone1("040 23287679")
				.build());
		}
		
	}
}
