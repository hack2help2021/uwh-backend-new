package com.uwh.api.response;


import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ramakrishna Veldandi
 * @project UWH
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentDto {
    private Long id;
    private OrganizationDto organization;
    private String uid;
    private String uidType;
    private String regdNo;
    private String studentName;
    private String gender;
    private String phone;
    private String additionalPhone;
    private String bplCard;
   // private Timestamp regdDate;
    private String regdDate;
    private String purpose;//Dropdown: College Placement Assistance/Hunar Course And Job/
    
    //private Timestamp dateOfBirth;
    private String dateOfBirth;
    
    private String qualification;
    private String additionalCourse;
    private String university;
    private Integer experience;
    private String field;
    private Double currentSalary;
    private String studentCurrentSkills;
    private String studentInterestedSkills;
    private String studentInterestedJobFields;
    private String studentJobLocationPreferences; 
    
    private Double expectedJobSalary;
    
    private String currentAddress;
    private String currentCity;
    private String districtName;
    private String pinCode;
            
    private String email;
    private String phone1;
    private String whatsAppNo2;
    
    private String referredByType; //drop down -> Self, Sponsored By - Honur/Colleague/FB/Friends/Other/Marketing Channel
    private String referredByName;
    
    private Set<CoursesDto> courses; 
}
