package com.uwh.api.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Ramakrishna Veldandi
 * @project UWH
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CoursesDto {
	private Long id;
	private String courseName;
	private String skills;
	private Integer duration;
	private Integer numberOfSeats; // 30
	private Integer numberOfEnrollment; // 25
	private Integer numberOf; // 25
	private String details;
	private String fileLocation;

	private String startDate;
	private String endDate;
    private PartnersDto partners;
}
