package com.uwh.api.response;


import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sirisha
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobsUpdateDto {
	
	@NotNull
    private Long id;
    private String jobType;//full-time/part-time
    private String jobName;// what could be the job Name?
    private String jobRole;//Typist/FrontDeskManager
    private String JobDescription;// Description about Job Duties, roles , day to day task.
    private String jobLocation; //HYD/Chennai
    private String jobIndustry;//HealthCare/Corporate/Repearing/jewellery/Teaching    
    private String minimumQualification;//Diploma/BSc/BA/BEd
    private String additionalQualification;//BEd,Vocational etc.
    private Double minSalary;
    private Double maxSalary;
    private Boolean travelRequired;
    private String jobPostingDate;    
    private String jobPostedByName;
    private String jobPostedByEmailId;
    private String jobPostedByMobileNo;
         
    private String jobValidateTillEndDate;
    private Integer jobOpenVacancy;
    
}
