package com.uwh.api.request;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author abdus
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginRequestDto {
    @NotBlank
    private String uid;

    @NotBlank
    private String uidType; //aadhar/pan/DL/mob no
    
    @NotBlank
    private String role; //student/admin
    
    
    @NotBlank
    private String password;
    
    

}
