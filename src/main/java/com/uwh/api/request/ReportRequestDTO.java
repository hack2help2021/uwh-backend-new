package com.uwh.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReportRequestDTO {
	
	private String centerName;
	private String gender;
	private RegistrationDate registrationDate;
	private String highestEducation;
	private String enrolledInCourse;
	private String experienceInYears;
	private String areaOfInterest;
	private String industry;
	private int offset;
	private int limit;
	public String getCenterName() {
		return centerName;
	}
	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public RegistrationDate getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(RegistrationDate registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getHighestEducation() {
		return highestEducation;
	}
	public void setHighestEducation(String highestEducation) {
		this.highestEducation = highestEducation;
	}
	public String getEnrolledInCourse() {
		return enrolledInCourse;
	}
	public void setEnrolledInCourse(String enrolledInCourse) {
		this.enrolledInCourse = enrolledInCourse;
	}
	public String getExperienceInYears() {
		return experienceInYears;
	}
	public void setExperienceInYears(String experienceInYears) {
		this.experienceInYears = experienceInYears;
	}
	public String getAreaOfInterest() {
		return areaOfInterest;
	}
	public void setAreaOfInterest(String areaOfInterest) {
		this.areaOfInterest = areaOfInterest;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	
	
	

}
