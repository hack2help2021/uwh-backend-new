package com.uwh.repositories;

import java.util.ArrayList;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.MapAttribute;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.uwh.api.request.ReportRequestDTO;
import com.uwh.api.response.StudentsDetailsDto;
import com.uwh.domain.Batches;
import com.uwh.domain.Courses;
import com.uwh.domain.Job;
import com.uwh.domain.Organization;
import com.uwh.domain.Partners;
import com.uwh.domain.Student;
import com.uwh.domain.StudentCourse;
import com.uwh.domain.StudentDetails;
import com.uwh.domain.StudentJobDetails;
import com.uwh.domain.StudentPage;

@Repository
public class StudentCriteriaRepository {
	
	private final EntityManager entityManager;
	private final CriteriaBuilder criteriaBuilder;
	
	
	public StudentCriteriaRepository(EntityManager em) {
		this.entityManager = em;
		this.criteriaBuilder = em.getCriteriaBuilder();
	}
	
	
	public Page<StudentDetails> findAllWithFilters (StudentPage studentPage, 
			ReportRequestDTO reportRequestDTO) {
		try {
			CriteriaQuery<StudentDetails> criteriaQuery = criteriaBuilder.createQuery(StudentDetails.class);
			Root<Student> studentRoot = criteriaQuery.from(Student.class);
			//Root<StudentCourse> studentCourseRoot = criteriaQuery.from(StudentCourse.class);
			//Root<StudentJobDetails> studentJobDetailsRoot = criteriaQuery.from(StudentJobDetails.class);
			//Root<StudentJobDetails> studentJobRoot = criteriaQuery.from(StudentJobDetails.class);
			//Root<Student> sRoot = criteriaQuery.from(Student.class);
			//Root<Courses> cRoot = criteriaQuery.from(Courses.class);
			//Join<Student, StudentCourse> stuCourseJoin = studentRoot.join("courses", JoinType.LEFT);
			//Join<Student, StudentCourse> courseJoin = studentRoot.join("courses", JoinType.LEFT);
			//Join<Student, StudentCourse> _c = cRoot.join("studentCourse");
			Join<Student, Courses> courseJoin = studentRoot.join("courses", JoinType.LEFT).join("courses", JoinType.LEFT);
			Join<Student, Partners> partnerJoin = courseJoin.join("partners", JoinType.LEFT);
			Join<Student, Organization> organizationJoin = partnerJoin.join("organization", JoinType.LEFT);
			Join<Student, StudentJobDetails> jobStudentJoin = studentRoot.join("jobs", JoinType.LEFT);
			Join<StudentJobDetails, Job> jobJoin = jobStudentJoin.join("job", JoinType.LEFT);
			//Join<Student, Job> jobJoin = studentJobDetailsRoot.join("job", JoinType.LEFT);
			//Join<Student, Job> jobJoin = studentJobRoot.join("job");
			Path<Courses> cPath = courseJoin.get("courseName");
			criteriaQuery.multiselect(studentRoot, 
					cPath,
					studentRoot.join("courses", JoinType.LEFT).get("status").alias("status"), 
					partnerJoin.get("partnerName"),
					organizationJoin.get("orgName"),
					jobStudentJoin.get("status").alias("jobStatus"),
					jobJoin.get("jobName"),
					jobJoin.get("jobType").alias("jobType"),
					jobJoin.get("jobLocation").alias("jobLocation"),
					jobJoin.get("JobDescription").alias("jobDesciption"),
					jobJoin.get("maxSalary").alias("maxSalary")
					/*jobJoin.get("jobLocation").alias("jobLocation"),
					jobJoin.get("maxSalary").alias("maxSalary"),
					jobJoin.get("jobType").alias("jobType")*/
					);
			
			/*
			 * criteriaQuery.multiselect(studentRoot, cRoot.
			 * cRoot.get("courseName").alias("courseName"),
			 * courseJoin.get("status").alias("status"));
			 */
			
			
			Predicate predicate = getPredicate (reportRequestDTO, studentRoot, organizationJoin);
			criteriaQuery.where(predicate);
			setOrder(studentPage, criteriaQuery, studentRoot);
			
			TypedQuery<StudentDetails> typedQuery = entityManager.createQuery(criteriaQuery);
			typedQuery.setFirstResult(studentPage.getPageNumber() * studentPage.getPageSize());
			typedQuery.setMaxResults(studentPage.getPageSize());
			
			Pageable pageable = getPageable (studentPage);
			
			long studentsCount = getStudentsCount(predicate);
			Page<StudentDetails> temp = new PageImpl<>(typedQuery.getResultList(), pageable, studentsCount);
			/*
			 * Page<StudentsDetailsDto> pageStuden temp.getContent().forEach((p) -> {
			 * StudentsDetailsDto dto = StudentsDetailsDto.builder().build();
			 * BeanUtils.copyProperties(p.getStudent(), dto);
			 * dto.setCourseName(p.getCourseName()); dto.setStatus(p.getStatus());
			 * dto.setPartnerName(p.getPartnerName()); dto.setOrgName(p.getOrgName());
			 * dto.setJobDescription(p.getJobDescription());
			 * dto.setJobLocation(p.getJobLocation()); dto.setJobName(p.getJobName());
			 * dto.setJobStatus(p.getJobStatus()); dto.setJobType(p.getJobType());
			 * dto.setMaxSalary(p.getMaxSalary()); });
			 */
			
			System.out.println(temp.toString());
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	private long getStudentsCount(Predicate predicate) {
		CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
		Root<Student> countRoot = countQuery.from(Student.class);
		countQuery.select(criteriaBuilder.count(countRoot)).where(predicate);
		return entityManager.createQuery(countQuery).getSingleResult();
	}


	private Pageable getPageable(StudentPage studentPage) {
		Sort sort = Sort.by(studentPage.getSortDirection(), studentPage.getSortBy());
		return PageRequest.of(studentPage.getPageNumber(), studentPage.getPageSize(), sort);
	}


	private void setOrder(StudentPage studentPage, CriteriaQuery<StudentDetails> criteriaQuery, Root<Student> studentRoot) {
		if (studentPage.getSortDirection().equals(Sort.Direction.ASC)) {
			criteriaQuery.orderBy(criteriaBuilder.asc(studentRoot.get(studentPage.getSortBy())));
		}
		else {
			criteriaQuery.orderBy(criteriaBuilder.desc(studentRoot.get(studentPage.getSortBy())));
		}
	}


	private Predicate getPredicate(ReportRequestDTO reportRequestDTO, Root<Student> studentRoot, 
			Join<Student, Organization> organizationJoin) {
		List<Predicate> predicates = new ArrayList<Predicate>(); 
		System.out.println(reportRequestDTO.toString());
		
		if (Objects.nonNull(reportRequestDTO.getGender())) { predicates.add(
		          criteriaBuilder.equal(studentRoot.get("gender"),reportRequestDTO.getGender())
		          ); }
		         
		        /*
		         * System.out.println(studentRoot.get("organization").get("orgName"));
		         * 
		         * if (Objects.nonNull(reportRequestDTO.getCenterName())) { predicates.add(
		         * criteriaBuilder.equal(studentRoot.get("organization").get("orgName"),
		         * reportRequestDTO.getCenterName()) ); }
		         */
		          
		          if (Objects.nonNull(reportRequestDTO.getHighestEducation())) { predicates.add(
		                  criteriaBuilder.equal(studentRoot.get("qualification"),reportRequestDTO.getHighestEducation())
		                  ); 
		          }
		          
		          if (Objects.nonNull(reportRequestDTO.getExperienceInYears())) {
		        	  if (!reportRequestDTO.getExperienceInYears().equalsIgnoreCase("0"))
			        	  predicates.add(
			                  criteriaBuilder.equal(studentRoot.get("experience"),reportRequestDTO.getExperienceInYears())
			                  ); 
		          }
		          
		          if (Objects.nonNull(reportRequestDTO.getAreaOfInterest())) { predicates.add(
		                  criteriaBuilder.equal(studentRoot.get("studentInterestedJobFields"),reportRequestDTO.getAreaOfInterest())
		                  ); 
		          }
		          
		          if (Objects.nonNull(reportRequestDTO.getAreaOfInterest())) { predicates.add(
		                  criteriaBuilder.equal(studentRoot.get("studentInterestedJobFields"),reportRequestDTO.getAreaOfInterest())
		                  ); 
		          }
		          
		          if (Objects.nonNull(reportRequestDTO.getIndustry())) { predicates.add(
		                  criteriaBuilder.equal(studentRoot.get("studentCurrentJobFields"),reportRequestDTO.getIndustry())
		                  ); 
		          }
		          
		          if (Objects.nonNull(reportRequestDTO.getRegistrationDate())) { 
		              if (Objects.nonNull(reportRequestDTO.getRegistrationDate().getFrom()) && 
		                      Objects.nonNull(reportRequestDTO.getRegistrationDate().getTo())) {
		                  predicates.add(
		                          criteriaBuilder.between(studentRoot.get("regdDate"),reportRequestDTO.getRegistrationDate().getFrom(), 
		                                  reportRequestDTO.getRegistrationDate().getTo())
		                          ); 
		              }
		              
		          }
		
		
		return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
	}

}
