package com.uwh.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uwh.domain.Job;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
}
