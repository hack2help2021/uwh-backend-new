package com.uwh.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uwh.domain.StudentBatch;

@Repository
public interface StudentBatchRepository extends JpaRepository<StudentBatch, Long> {

//	Page<StudentCourse> findAllByCoursesIdAndStatus(Long id, String string, Pageable pageable);
}
