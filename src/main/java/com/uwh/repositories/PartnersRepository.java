package com.uwh.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uwh.domain.Partners;

@Repository
public interface PartnersRepository extends JpaRepository<Partners, Long> {

}
