package com.uwh.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uwh.domain.Courses;

@Repository
public interface CoursesRepository extends JpaRepository<Courses, Long> {

	List<Courses> findAllByPartnersId(Long partnerId);

}
