package com.uwh.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.uwh.api.response.BatchesDTO;
import com.uwh.api.response.CoursesDto;
import com.uwh.api.response.OrganizationDto;
import com.uwh.api.response.PartnersDto;
import com.uwh.api.response.StudentDto;
import com.uwh.domain.Batches;
import com.uwh.domain.Courses;
import com.uwh.domain.Partners;
import com.uwh.domain.StudentBatch;

public class DtoConverter {

	public static List<PartnersDto> convertToPartnerDto(List<Partners> partners) throws IllegalAccessException, InvocationTargetException {
		List<PartnersDto> partnersDtos = new ArrayList<PartnersDto>();
		for (Partners partner : partners) {
			PartnersDto partnersDto = new PartnersDto();
			BeanUtils.copyProperties(partner, partnersDto);
			if (partner.getOrganization() != null) {
				OrganizationDto organizationDto = new OrganizationDto();
				BeanUtils.copyProperties(partner.getOrganization(), organizationDto);
				partnersDto.setOrganization(organizationDto);
			}
			partnersDtos.add(partnersDto);
		}
		return partnersDtos;
	}

	public static List<CoursesDto> convertToCoursesDto(List<Courses> courses) throws IllegalAccessException, InvocationTargetException {
		List<CoursesDto> coursesDtos = new ArrayList<CoursesDto>();
		for (Courses course : courses) {
			CoursesDto coursesDto = new CoursesDto();
			BeanUtils.copyProperties(course, coursesDto);
			PartnersDto partnersDto = new PartnersDto();
			BeanUtils.copyProperties(course.getPartners(), partnersDto);
			coursesDto.setPartners(partnersDto);
			coursesDtos.add(coursesDto);
		}
		return coursesDtos;
	}
	
	
	public static List<BatchesDTO> convertToBatchesDto(List<Batches> batches) throws IllegalAccessException, InvocationTargetException {
		List<BatchesDTO> batchesDTOs = new ArrayList<BatchesDTO>();
		for (Batches batch : batches) {
			BatchesDTO batchesDTO = new BatchesDTO();
			BeanUtils.copyProperties(batch, batchesDTO);
			CoursesDto coursesDto = new CoursesDto();
			BeanUtils.copyProperties(batch.getCourses(), coursesDto);
			batchesDTO.setCourses(coursesDto);
			
			List<StudentDto> students = new ArrayList<StudentDto>();
			for (StudentBatch target : batch.getStudents()) {
				StudentDto dto = new StudentDto();
				BeanUtils.copyProperties(target.getStudent(), dto);
				students.add(dto);
			}
			batchesDTO.setStudents(students);
			batchesDTOs.add(batchesDTO);
		}
		return batchesDTOs;
	}
}
