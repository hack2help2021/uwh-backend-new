package com.uwh.utils;

public class Constant {
    public static final String API_VERSION = "v1";
    public static final String BASE_CONTEXT_PATH = "/uwh-service/" + API_VERSION;
}
