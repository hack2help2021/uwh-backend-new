package com.uwh.exceptions;

public class BadRequestException extends RuntimeException {
    /**
     *
     * @param message
     */
    public BadRequestException(String message) {
        super(message);
    }

    /**
     *
     */
    public BadRequestException() {
        super("Bad request");
    }
}
